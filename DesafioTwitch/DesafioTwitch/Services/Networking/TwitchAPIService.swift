
import Foundation
import Alamofire
import AlamofireObjectMapper

class TwitchAPIService {
    static func makeRequest(page: Int, onSuccess success: @escaping (_ result: TopGame) -> Void) {
        let headers: HTTPHeaders = [
            "Client-Id": "wy8qjkw90zdyy5ec0e6ib4iopd3iqi"
        ]
        
        Alamofire.request("https://api.twitch.tv/kraken/games/top?offset=" + String(page), headers: headers).responseObject { (response: DataResponse<TopGame>) in
            let top = response.result.value
            success(top!)
        }
    }
    
}
