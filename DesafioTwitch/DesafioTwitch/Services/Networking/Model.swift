
import Foundation
import ObjectMapper

class Game: Mappable {
    
    var gameName: String?
    var gameImage: String?
    var gameViewers: Int?
    var gamePropularity: Int?
    var gameImg: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        gameName          <- map["game.name"]
        gameImage         <- map["game.box.medium"]
        gameViewers       <- map["viewers"]
        gamePropularity   <- map["game.popularity"]
        gameImg          <- map["game.logo.medium"]
    }
}

class TopGame: Mappable{
    
    var topGame: [Game]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        topGame          <- map["top"]
    }
}



