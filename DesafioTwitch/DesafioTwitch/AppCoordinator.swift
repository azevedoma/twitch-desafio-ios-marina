

import Foundation
import UIKit

class AppCoordinator {
    class func startViewController (newVC: UIViewController, parent: Any) {
        if let window = parent as? UIWindow {
            window.rootViewController = newVC
            return
        }
        if let nav = parent as? UINavigationController {
            nav.pushViewController(newVC, animated: true)
            return
        }
        if let controller = parent as? UIViewController {
            controller.present(newVC, animated: true, completion: {})
            return
        }
    }
    
    
    class func goToMainView (parent: Any) {
        let listVC = ListViewController(nibName: "ListViewController", bundle: nil)
        let nav = UINavigationController(rootViewController: listVC)
        nav.setNavigationBarHidden(true, animated: false)
        nav.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        nav.navigationBar.shadowImage = UIImage()
        nav.navigationBar.isTranslucent = false
        nav.view.backgroundColor = UIColor.clear
        AppCoordinator.startViewController(newVC: nav, parent: parent)
    }
    
    
    class func goToDetail (parent: Any) {
        let DetailVC = DetailViewController(nibName: "DetailViewController", bundle: nil)
        AppCoordinator.startViewController(newVC: DetailVC, parent: parent)
    }
}
