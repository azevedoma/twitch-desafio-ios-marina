

import UIKit
import Reusable

enum LoadingStatus {
    case loading
    case didLoadAll
    case moreToLoad
}

class ListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var totalItems = 0
    var delegate: ListViewDelegate?
    var cells: [UITableViewCell] = []
    var viewModel: ListViewModel?
    var numberOfCells = 15
    var loadingStatus = LoadingStatus.moreToLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.title = "MOST POPULAR GAMES"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func setupTableView() {
        tableView.register(cellType: ListViewCell.self)
        
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.delegate = self
        tableView.dataSource = self
        
        self.viewModel = ListViewModel()
    
    }
    
    
    func reloadData() {
        tableView?.reloadData()
        if numberOfCells > 0 {
            reloadData()
        }
    }
    
    func loadMoreGames() {
        if numberOfCells >= 40 {
            loadingStatus = .didLoadAll
            tableView?.reloadData()
            return
        }
    }
}

extension ListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if let gameCard: Game = viewModel?.itemForIndex(indexPath.row) {
            let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ListViewCell.self)
            cell.configCell(gameInfo: gameCard, rankingNumber: indexPath.row + 1)
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
}

func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60.0
}
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let selectedCell = cells[indexPath.row]
        if selectedCell is ListViewCell {
         AppCoordinator.goToDetail(parent: self.navigationController)
      }
    }

}


