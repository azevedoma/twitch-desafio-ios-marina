import Foundation
import UIKit

protocol ListViewFeedback: class {
    func didLoadGames()
}

protocol ListViewDataSource {
    func itemForIndex(_ index: Int) -> Game?
    func getItems() -> [Game]
}

protocol ListViewDelegate {
    func loadMoreGames()
}

class ListViewModel: ListViewDataSource {
    
    func itemForIndex(_ index: Int) -> Game? {
        if index < itemList.count {
            return itemList[index]
        } else {
            return nil
        }
    }
    
    func getItems() -> [Game] {
        return itemList
    }
    
    var itemList = [Game]()
    var listViewFeedback: ListViewFeedback?
    var page: Int = 0
    
    class func newWithFeedback(listViewFeedback: ListViewFeedback) -> ListViewModel {
        let instance = ListViewModel()
        instance.listViewFeedback = listViewFeedback
        return instance
    }
}


extension ListViewModel: ListViewDelegate {
    func loadMoreGames() {
        TwitchAPIService.makeRequest(page: page, onSuccess: { (result) in
            self.page = self.page + 15
            guard let result = result.topGame else { return }
            for item in result {
                self.itemList.append(item)
            }
            self.listViewFeedback?.didLoadGames()
        })
        
    }
}
