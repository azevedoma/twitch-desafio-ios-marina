

import UIKit
import AlamofireImage

class DetailViewController: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var viewTitle: UILabel!
    @IBOutlet weak var gameTitle: UILabel!
    @IBOutlet weak var gameImg: UIImageView!
    @IBOutlet weak var popularityLabel: UILabel!
    @IBOutlet weak var viewers: UILabel!
    
    var game: Game?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func SetupDetailView() {
        guard let game = game else { return }
        gameTitle.text = game.gameName
        let gameTotalViewers: Int = game.gameViewers ?? 0
        viewers.text = "\(gameTotalViewers)"
        guard let url = game.gameImage else { return }
        gameImg.af_setImage(withURL: URL(string: url)!)
        let gamePopularity: Int = game.gamePropularity ?? 0
        popularityLabel.text = "\(gamePopularity)"
    }
}
