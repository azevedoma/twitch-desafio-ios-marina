

import UIKit
import Reusable
import AlamofireImage

class ListViewCell: UITableViewCell, NibReusable {
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var bottomLine: UIView!
    @IBOutlet weak var ranking: UILabel!
    @IBOutlet weak var imageGame: UIImageView!
    @IBOutlet weak var gameName: UILabel!
    @IBOutlet weak var viewers: UILabel!
    
    var gameCard: Game?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
        func configCell(gameInfo: Game, rankingNumber: Int) {
            self.gameCard = gameInfo
            guard let gameCard = gameCard else { return }
            gameName.text = gameCard.gameName
            let gameTotalViewers: Int = gameCard.gameViewers ?? 0
            viewers.text = "\(gameTotalViewers)" + " viewers"
            guard let url = gameCard.gameImage else { return }
            imageGame.af_setImage(withURL: URL(string: url)!, placeholderImage: nil)
            ranking.text = "\(rankingNumber)"
        }
    }

